package com.epam;

public interface CalculatorService {

        public void printInput(int[] value);

        public default int[] printLargestPlateau(int[] values){ int biggestStartIndex = -1;
            int biggestLenth = 0;
            int currentIndex = 1;
            int currentPlateauStartIndex = 1;
            int currentLenght = 1;
            boolean plateauStarted = false;
            while (currentIndex < values.length) {
                if (isStartOfPlateau(currentIndex, values)) {
                    plateauStarted = true;
                    currentPlateauStartIndex = currentIndex;
                } else if (isEndOfPlateau(currentIndex, values)) {
                    if (plateauStarted && currentLenght > biggestLenth) {
                        biggestLenth = currentLenght;
                        biggestStartIndex = currentPlateauStartIndex;
                    }
                    plateauStarted = false;
                    currentLenght = 1;
                } else {
                    currentLenght++;
                }
                currentIndex++;
            }
            if (biggestStartIndex < 0) {
                System.out.println("No plateau");
            } else {
                System.out.println("Biggest plateau starts at index: " + biggestStartIndex + " and has length: " + biggestLenth);
            }
            int[] arr = new int[]{biggestStartIndex, biggestLenth};
            return arr;}

        public boolean isStartOfPlateau(int index, int[] values);

        public boolean isEndOfPlateau(int index, int[] values);


}
