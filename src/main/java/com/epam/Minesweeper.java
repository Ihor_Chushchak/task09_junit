package com.epam;

/*************************************************************************
 *  Compilation:  javac Minesweeper.java
 *  Execution:    java Minesweeper M N p
 *
 *  Creates an MxN minesweeper game where each cell is a bomb with
 *  probability p. Prints out the MxN game and the neighboring bomb
 *  counts.
 *
 *  Sample execution:
 *
 *      % java Minesweeper  5 10 0.3
 *      * . . . . . . . . *
 *      . . . . . . * . . .
 *      . . . . . . . . * *
 *      . . . * * * . . * .
 *      . . . * . . . . . .
 *
 *      * 1 0 0 0 1 1 1 1 *
 *      1 1 0 0 0 1 * 2 3 3
 *      0 0 1 2 3 3 2 3 * *
 *      0 0 2 * * * 1 2 * 3
 *      0 0 2 * 4 2 1 1 1 1
 *
 *
 *************************************************************************/
public class Minesweeper {

    public static void printInput(int M, int N, boolean[][] bombs) {
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++)
                if (bombs[i][j]) System.out.print("* ");
                else System.out.print(". ");
            System.out.println();
        }
    }

    public static void printOutput(int M, int N, boolean[][] bombs, int[][] sol) {
        System.out.println();
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++)
                if (bombs[i][j]) System.out.print("* ");
                else System.out.print(sol[i][j] + " ");
            System.out.println();
        }
    }

    public static boolean[][] fillBombsArray(int M, int N, boolean[][] bombs, double p) {
        for (int i = 1; i <= M; i++)
            for (int j = 1; j <= N; j++)
                bombs[i][j] = (Math.random() < p);
        return bombs;
    }
    static int[][] fillSolArray(int M, int N, boolean[][] bombs, int[][] sol){
        for (int i = 1; i <= M; i++)
            for (int j = 1; j <= N; j++)
                // (ii, jj) indexes neighboring cells
                for (int ii = i - 1; ii <= i + 1; ii++)
                    for (int jj = j - 1; jj <= j + 1; jj++)
                        if (bombs[ii][jj]) sol[i][j]++;
        return sol;
    }

    public static void main(String[] args) {
        int M = 5;
        int N = 6;
        double p = 0.86;

        // game grid is [1..M][1..N], border is used to handle boundary cases
        boolean[][] bombs = new boolean[M + 2][N + 2];
        fillBombsArray(M,N,bombs,p);
        // print game
        printInput(M, N, bombs);
        // sol[i][j] = # bombs adjacent to cell (i, j)
        int[][] sol = new int[M + 2][N + 2];
        fillSolArray(M, N, bombs, sol);

        // print solution
        printOutput(M, N, bombs, sol);

    }
}