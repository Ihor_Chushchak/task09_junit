package com.epam;

public class PlateauFinder {

    private CalculatorService calcService;

    public PlateauFinder(CalculatorService calcService) {
        this.calcService = calcService;
    }


    public static void printInput(int[] value) {
        for (int i = 0; i < value.length; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i = 0; i < value.length; i++) {
            System.out.print(value[i] + " ");
        }
        System.out.println();
    }

    public static int[] printLargestPlateau(int[] values) {
        int biggestStartIndex = -1;
        int biggestLenth = 0;
        int currentIndex = 1;
        int currentPlateauStartIndex = 1;
        int currentLenght = 1;
        boolean plateauStarted = false;
        while (currentIndex < values.length) {
            if (isStartOfPlateau(currentIndex, values)) {
                plateauStarted = true;
                currentPlateauStartIndex = currentIndex;
            } else if (isEndOfPlateau(currentIndex, values)) {
                if (plateauStarted && currentLenght > biggestLenth) {
                    biggestLenth = currentLenght;
                    biggestStartIndex = currentPlateauStartIndex;
                }
                plateauStarted = false;
                currentLenght = 1;
            } else {
                currentLenght++;
            }
            currentIndex++;
        }
        if (biggestStartIndex < 0) {
            System.out.println("No plateau");
        } else {
            System.out.println("Biggest plateau starts at index: " + biggestStartIndex + " and has length: " + biggestLenth);
        }
        int[] arr = new int[]{biggestStartIndex, biggestLenth};
        return arr;
    }

    private static boolean isStartOfPlateau(int index, int[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] < values[index];
    }

    private static boolean isEndOfPlateau(int index, int[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] > values[index];
    }

    public static void main(String[] args) {
        int[] values = new int[]{2, 2, 2, 1, 2, 3, 3, 2, 1};
        printInput(values);
        printLargestPlateau(values);
    }
}