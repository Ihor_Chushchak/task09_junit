package com.epam.tests;

import com.epam.Minesweeper;
import com.epam.PlateauFinder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;
import java.util.ResourceBundle;

import static com.epam.PlateauFinder.printLargestPlateau;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DisplayName("Plateau Finder Test")
@ExtendWith(MockitoExtension.class)
public class PlateauFinderTest {

    @Mock
    PlateauFinder plateauFinder;

    @DisplayName("Largest Plateau Test")
    @Test
    public void printLargestPlateauTest() {
        int[] array = new int[]{0, 1, 1, 0};
        int[] expectRes = new int[]{1, 2};
        int[] result = printLargestPlateau(array);
        assertArrayEquals(result, expectRes);
        System.out.println("Is done");

        when(plateauFinder.printLargestPlateau(array)).thenReturn(expectRes);

        verify(plateauFinder).printLargestPlateau(array);
    }
}
