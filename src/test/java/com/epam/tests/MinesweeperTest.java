package com.epam.tests;

import com.epam.Minesweeper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.mockito.Mock;

import static com.epam.Minesweeper.fillBombsArray;

@DisplayName("Minesweeper Test")
public class MinesweeperTest {

    @Mock
    Minesweeper minesweeper;

    @DisplayName("Fill Bombs Array Test")
    @RepeatedTest(20)
    public void fillBombsArrayTest() {
        int M = 6;
        int N = 5;
        double p = 0.86;
        boolean[][] bombs = new boolean[M + 2][N + 2];
        fillBombsArray(M,N,bombs,p);
        Assert.assertNotNull(bombs);
        System.out.println("Is done");
    }

}
